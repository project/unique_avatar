
Unique Avatar Module
==========================================
by Jared A. Scheel (http://drupal.org/user/116197)

Unique Avatar gets around browser caching of user pictures by creating a 
unique filename each time the image is uploaded. When a user picture is 
uploaded, a 32-character unique is appended to the the filename.

Unique Avatar also flushes any ImageCache (http://drupal.org/project/imagecache) 
caches of the existing user picture.

Funded by centre{source} (http://www.centresource.com)
